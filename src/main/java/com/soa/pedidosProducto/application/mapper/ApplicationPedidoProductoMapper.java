package com.soa.pedidosProducto.application.mapper;

import com.soa.pedidosProducto.application.model.GuardarPedidoProducto;
import com.soa.pedidosProducto.application.model.ObtenerPedidoProducto;
import com.soa.pedidosProducto.domain.model.PedidoProducto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring")
public interface ApplicationPedidoProductoMapper {

    @Mappings({
            @Mapping(source = "productoID", target = "producto.productoID")
    })
    PedidoProducto guardarPedidoProductoToPedidoProducto(GuardarPedidoProducto guardarPedidoProducto);

    @Mappings({
            @Mapping(source = "producto.productoID", target = "productoID")
    })
    GuardarPedidoProducto pedidoProductoToGuardarPedidoProducto(PedidoProducto pedidoProducto);


    PedidoProducto obtenerPedidoProductoToPedidoProducto(ObtenerPedidoProducto obtenerPedidoProducto);


    @Mappings({
            @Mapping(source = "producto.productoID", target = "productoID"),
            @Mapping(source = "producto.nombre", target = "nombre"),
            @Mapping(source = "producto.precioVenta", target = "precioVenta"),
            @Mapping(source = "producto.fechaCaducidad", target = "fechaCaducidad")
    })
    ObtenerPedidoProducto pedidoProductoToObtenerPedidoProducto(PedidoProducto pedidoProducto);


}
