package com.soa.pedidosProducto.application.service;


import com.soa.pedidosProducto.application.api.ProductoApiDelegate;
import com.soa.pedidosProducto.application.mapper.ApplicationPedidoProductoMapper;
import com.soa.pedidosProducto.application.model.GuardarPedidoProducto;
import com.soa.pedidosProducto.application.model.ObtenerPedidoProducto;
import com.soa.pedidosProducto.domain.service.DomainPedidoProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.List;


@Component
public class ApplicationPedidoProductoServiceImpl implements ProductoApiDelegate {

    @Autowired
    private ApplicationPedidoProductoMapper applicationPedidoProductoMapper;

    @Autowired
    private DomainPedidoProductoService domainPedidoProductoService;


    @Override
    public ResponseEntity<List<GuardarPedidoProducto>> guardarPedidoProductos(
            List<GuardarPedidoProducto> guardarPedidoProducto
    )  {
        var productos = domainPedidoProductoService.savePedidoProductos(
                guardarPedidoProducto.stream()
                        .map(applicationPedidoProductoMapper::guardarPedidoProductoToPedidoProducto)
                        .toList()
        );

        return ResponseEntity.ok(
                productos.stream()
                        .map(applicationPedidoProductoMapper::pedidoProductoToGuardarPedidoProducto)
                        .toList()
        );
    }

    @Override
    public ResponseEntity<List<ObtenerPedidoProducto>> obtenerPedidoProductos(
            ObtenerPedidoProducto parametros
    ) {
        var productos = domainPedidoProductoService.getPedidoProductos(
                applicationPedidoProductoMapper.obtenerPedidoProductoToPedidoProducto(parametros)
        );

        return ResponseEntity.ok(
                productos.stream()
                        .map(applicationPedidoProductoMapper::pedidoProductoToObtenerPedidoProducto)
                        .toList()
        );
    }

}

