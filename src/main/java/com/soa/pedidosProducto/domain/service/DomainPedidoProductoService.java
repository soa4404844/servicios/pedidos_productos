package com.soa.pedidosProducto.domain.service;

import com.soa.pedidosProducto.domain.model.PedidoProducto;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DomainPedidoProductoService {

    List<PedidoProducto> savePedidoProductos(List<PedidoProducto> pedidoProductos);

    List<PedidoProducto> getPedidoProductos(PedidoProducto parametros);

}
