package com.soa.pedidosProducto.domain.service;

import com.soa.pedidosProducto.domain.model.PedidoProducto;
import com.soa.pedidosProducto.domain.repository.DomainPedidoProductoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.StreamSupport;

@Service
public class DomainPedidoProductoServiceImpl implements DomainPedidoProductoService {

    @Autowired
    private DomainPedidoProductoRepository domainPedidoProductoRepository;


    @Override
    public List<PedidoProducto> savePedidoProductos(List<PedidoProducto> pedidoProductos) {

        var saveAllResult = domainPedidoProductoRepository.saveAll(pedidoProductos);

        return StreamSupport.stream(saveAllResult.spliterator(), true)
                .toList();
    }

    @Override
    public List<PedidoProducto> getPedidoProductos(PedidoProducto parametros) {
        return domainPedidoProductoRepository.findByPorParametro(parametros);
    }
}
