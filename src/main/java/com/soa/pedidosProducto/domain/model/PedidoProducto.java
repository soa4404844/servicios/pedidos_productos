package com.soa.pedidosProducto.domain.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "PEDIDO_PRODUCTOS")
public class PedidoProducto {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "pedido_producto_id")
    private Integer pedidoProductoID;

    @Column(name = "pedido_id")
    private Integer pedidoID;

    @Column(name = "cantidad")
    private Double cantidad;

    @OneToOne
    @JoinColumn(name = "producto_id")
    private Producto producto;

}