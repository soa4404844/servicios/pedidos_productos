package com.soa.pedidosProducto.domain.repository;

import com.soa.pedidosProducto.domain.model.PedidoProducto;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DomainPedidoProductoRepository extends CrudRepository<PedidoProducto, Integer> {

    @Query("""
            SELECT PP FROM PEDIDO_PRODUCTOS PP
            WHERE (:#{#pedidoProductoTabla.pedidoID} IS NULL OR PP.pedidoID = :#{#pedidoProductoTabla.pedidoID})
            """)
    List<PedidoProducto> findByPorParametro(
            @Param("pedidoProductoTabla") PedidoProducto pedidoProducto
    );

}
